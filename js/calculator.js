// const calculator = {
//   displayValue: '0',
//   firstOperand: null,
//   waitingForSecondOperand: false,
//   operator: null,
// };

// function inputDigit(digit) {
//   const { displayValue, waitingForSecondOperand } = calculator;

//   if (waitingForSecondOperand === true) {
//     calculator.displayValue = digit;
//     calculator.waitingForSecondOperand = false;
//   } else {
//     calculator.displayValue = displayValue === '0' ? digit : displayValue + digit;
//   }
// }

// function inputDecimal(dot) {
//   // If the `displayValue` does not contain a decimal point
//   if (!calculator.displayValue.includes(dot)) {
//     // Append the decimal point
//     calculator.displayValue += dot;
//   }
// }

// function handleOperator(nextOperator) {
//   const { firstOperand, displayValue, operator } = calculator
//   const inputValue = parseFloat(displayValue);

//   if (operator && calculator.waitingForSecondOperand)  {
//     calculator.operator = nextOperator;
//     return;
//   }

//   if (firstOperand == null) {
//     calculator.firstOperand = inputValue;
//   } else if (operator) {
//     const currentValue = firstOperand || 0;
//     const result = performCalculation[operator](currentValue, inputValue);

//     calculator.displayValue = String(result);
//     calculator.firstOperand = result;
//   }

//   calculator.waitingForSecondOperand = true;
//   calculator.operator = nextOperator;
// }

// const performCalculation = {
//   '/': (firstOperand, secondOperand) => firstOperand / secondOperand,

//   '*': (firstOperand, secondOperand) => firstOperand * secondOperand,

//   '+': (firstOperand, secondOperand) => firstOperand + secondOperand,

//   '-': (firstOperand, secondOperand) => firstOperand - secondOperand,

//   '=': (firstOperand, secondOperand) => secondOperand
// };

// function resetCalculator() {
//   calculator.displayValue = '0';
//   calculator.firstOperand = null;
//   calculator.waitingForSecondOperand = false;
//   calculator.operator = null;
// }

// function updateDisplay() {
//   const display = document.querySelector('.calculator-screen');
//   display.value = calculator.displayValue;
// }

// updateDisplay();

// const keys = document.querySelector('.calculator-keys');
// keys.addEventListener('click', (event) => {
//   const { target } = event;
//   if (!target.matches('button')) {
//     return;
//   }

//   if (target.classList.contains('operator')) {
//     handleOperator(target.value);
//     updateDisplay();
//     return;
//   }

//   if (target.classList.contains('decimal')) {
//     inputDecimal(target.value);
//     updateDisplay();
//     return;
//   }

//   if (target.classList.contains('all-clear')) {
//     resetCalculator();
//     updateDisplay();
//     return;
//   }

//   inputDigit(target.value);
//   updateDisplay();
// });

jQuery(document).ready(function(){
  //KeyDown Events
  jQuery(document).keydown(function(e){
    //ON KEY PRESS Ctrl + Alt + C on customer MOdal
    if( e.which === 67 && e.ctrlKey && e.altKey ){
      jQuery("#customerNames").trigger("click");
    }

    //ON KEY PRESS Ctrl + Alt + I on Items MOdal
    if( e.which === 73 && e.ctrlKey && e.altKey ){
      jQuery("#itemNames").trigger("click");
    }

    //ON KEY PRESS Ctrl + Alt + P on PRint and Generate
    if( e.which === 80 && e.ctrlKey && e.altKey ){
      jQuery("#print-generate-bill").trigger("click");
    }

    //ON KEY PRESS Ctrl + Alt + G on Generate BIll
    if( e.which === 71 && e.ctrlKey && e.altKey ){
      jQuery("#generate-bill").trigger("click");
    }

    //ON KEY PRESS Ctrl + Alt + Z on Cacel BIll
    if( e.which === 90 && e.ctrlKey && e.altKey ){
      jQuery("#cancel-bill").trigger("click");
    }
  }); 

  //Print BIll BUtton
  jQuery('#print-generate-bill').click(function(){
    jQuery.print(".description-table");
  });

  //Generate BIll
  jQuery('#generate-bill').click(function(){
    alert("Generate Bill Clicked");
  });

  //Cancel BIll BUtton
  jQuery('#cancel-bill').click(function(){
    alert("Cancel Clicked");
  });


  //Getting Selected Customer to the Invoice
  jQuery('.customer-row').click(function(){
    var customerID = jQuery(this).data('ta-customer-id');
    jQuery ("#customerNames").val(jQuery('[data-ta-customer-id='+ customerID +']').find('.customername').html());
    jQuery(".close").trigger('click');
  });

  //Item Quantity
  jQuery("input.item-qty").change(function(e){
    if(jQuery(this).val() < 1 ){
      jQuery(this).val(1);
    }
  }); 






  // 
  // 
  // Main Right Bar Buttons Actions
  // 
  // 
  jQuery(".dynamic-content").hide();
  jQuery(".main-page").show(); // Initial Show HomePage 

  //Billing History
  jQuery("#billing-history-btn").click(function(){
    jQuery(".dynamic-content").hide();
    jQuery(".billing").show();
  });

  //Sales Report
  jQuery("#sales-report-btn").click(function(){
    jQuery(".dynamic-content").hide();
    jQuery(".sales-report").show();
  });

  //Payments
  jQuery("#payments-btn").click(function(){
    jQuery(".dynamic-content").hide();
    jQuery(".payments").show();
  });

  //Customers
  jQuery("#customers-btn").click(function(){
    jQuery(".dynamic-content").hide();
    jQuery(".customer").show();
  });

  //Manufacturers
  jQuery("#manufacturers-btn").click(function(){
    jQuery(".dynamic-content").hide();
    jQuery(".manufacturers").show();
  });

  //inventory
  jQuery("#inventory-btn").click(function(){
    jQuery(".dynamic-content").hide();
    jQuery(".inventory").show();
  });

  //Suppliers
  jQuery("#suppliers-btn").click(function(){
    jQuery(".dynamic-content").hide();
    jQuery(".suppliers").show();
  });
});